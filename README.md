# Rclone Debian & Ubuntu package repository

[rclone](https://rclone.org/) apt repository hosted on Gitlab Pages.

Gitlab CI downloads the latest Debian package version from the [rclone Github page](https://github.com/rclone/rclone/releases) each night.

## How to install

### Add repo signing key to apt

```sh
sudo curl -sL -o /etc/apt/trusted.gpg.d/cr1xu5-rclone.asc https://cr1xu5.gitlab.io/rclone-debian-repository/gpg.key
```

### Add repo to apt

```sh
echo "deb https://cr1xu5.gitlab.io/rclone-debian-repository rclone main" | sudo tee /etc/apt/sources.list.d/cr1xu5-rclone.list
```

Available architectures are almost equal to those provided upstream:

- `amd64`
- `arm64`
- `armel`
- `armhf` (we chose the ARMv6 architecture as it also runs on ARMv7; see [upstream issue 5107](https://github.com/rclone/rclone/issues/5107#issuecomment-811413938))
- `i386`
- `mips`
- `mipsel`

### Install

```sh
sudo apt update
sudo apt install rclone
```

## Integrity

You can compare the SHA256SUM of the Debian package [in the generated Debian repository](https://cr1xu5.gitlab.io/rclone-debian-repository/dists/rclone/main/binary-amd64/Packages) with [the checksum provided on the rclone Github releases page](https://github.com/rclone/rclone/releases/latest).

## Credits

Inspired by <https://gitlab.com/packaging> and their [reprepro Docker image](https://gitlab.com/packaging/reprepro-multiple-versions).
